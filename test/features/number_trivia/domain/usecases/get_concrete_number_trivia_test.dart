import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:trivia_app/core/usecases/usecases.dart';
import 'package:trivia_app/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:trivia_app/features/number_trivia/domain/repositories/trivia_number_repository.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:trivia_app/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';

class MockNumberTriviaRepository extends Mock
    implements NumberTriviaRepository {}

void main() {
  GetConcreteNumberTrivia usecase;
  MockNumberTriviaRepository mockNumberTriviaRepository;

  setUp(() {
    mockNumberTriviaRepository = MockNumberTriviaRepository();
    usecase = GetConcreteNumberTrivia(mockNumberTriviaRepository);
  });

  final tnumber = 1;
  final tnumberTrivia = NumberTrivia(number: 1, text: "test");

  test('should get trivia for the number from repo', () async {
    // arrange
    when(mockNumberTriviaRepository.getConcreteNumberTrivia(any))
        .thenAnswer((_) async => Right(tnumberTrivia));
    // act
    final result = await usecase(Params(number: tnumber));
    // assert
    expect(result, Right(tnumberTrivia));
    verify(mockNumberTriviaRepository.getConcreteNumberTrivia(tnumber));
    verifyNoMoreInteractions(mockNumberTriviaRepository);
  });
}
