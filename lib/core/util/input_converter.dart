import 'package:dartz/dartz.dart';
import 'package:trivia_app/core/errors/failures.dart';

class InputConverter {
  Either<Failure, int> stringToUsignedInteget(String str) {
    try {
      final integer = int.parse(str);
      if (integer < 0) {
        throw FormatException();
      }
      return Right(integer);
    } on FormatException {
      return Left(InvalidInputFailure());
    }
  }
}

class InvalidInputFailure extends Failure {}
