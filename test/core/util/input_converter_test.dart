import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:trivia_app/core/util/input_converter.dart';

main() {
  InputConverter inputConverter;

  setUp(() {
    inputConverter = InputConverter();
  });

  group('stringToUsignedInt', () {
    test('should return a integet when the string represents integer',
        () async {
      // arrange
      final str = '123';
      // act
      final result = inputConverter.stringToUsignedInteget(str);
      // assert
      expect(result, Right(123));
    });

    test('should return a Faliure when the string is not an integer', () async {
      // arrange
      final str = 'abc';
      // act
      final result = inputConverter.stringToUsignedInteget(str);
      // assert
      expect(result, Left(InvalidInputFailure()));
    });

        test('should return a Faliure when the string is a negative integer', () async {
      // arrange
      final str = '-132';
      // act
      final result = inputConverter.stringToUsignedInteget(str);
      // assert
      expect(result, Left(InvalidInputFailure()));
    });
  });
}
