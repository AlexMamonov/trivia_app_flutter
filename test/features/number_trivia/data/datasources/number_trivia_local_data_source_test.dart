import 'dart:convert';

import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trivia_app/core/errors/exceptions.dart';
import 'package:trivia_app/features/number_trivia/data/datasources/number_trivia_local_data_source.dart';
import 'package:trivia_app/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:matcher/matcher.dart';
import '../../../../fixtures/fixture_reader.dart';

class MockSharepPreferences extends Mock implements SharedPreferences {}

void main() {
  NumberTriviaLocalDataSourceImlp dataSource;
  MockSharepPreferences mockSharepPreferences;

  setUp(() {
    mockSharepPreferences = MockSharepPreferences();
    dataSource = NumberTriviaLocalDataSourceImlp(
        sharedPreferences: mockSharepPreferences);
  });

  group("GetLastNumberTrivia", () {
    final tNumberTriviaModel =
        NumberTriviaModel.fromJson(json.decode(fixture("trivia_cached.json")));
    test(
        'should return NumberTrivia from shared pres when there is one in the cache',
        () async {
      // arrange
      when(mockSharepPreferences.getString(any))
          .thenReturn(fixture("trivia_cached.json"));
      // act
      final result = await dataSource.getLastNumberTrivia();
      // assert
      verify(mockSharepPreferences.getString(CACHED_NUMBER_TRIVIA));
      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw CacheExeption when there in not a cached value',
        () async {
      // arrange
      when(mockSharepPreferences.getString(any)).thenReturn(null);
      // act
      final call = dataSource.getLastNumberTrivia;
      // assert
      expect(() => call(), throwsA(TypeMatcher<CacheException>()));
    });
  });

  group("CacheNumberTrivia", () {
    final tNumberTriviaModel = NumberTriviaModel(number: 1, text: 'test Text');
    test('should call shared prefs to cache the data', () async {
      // act
      dataSource.cacheNumberTrivia(tNumberTriviaModel);
      // assert
      final expectedJsonString = json.encode(tNumberTriviaModel.toJson());
      verify(mockSharepPreferences.setString(
          CACHED_NUMBER_TRIVIA, expectedJsonString));
    });
  });
}
